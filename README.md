# Vue / Kemal Boilerplate

## Commands

### Using Make

Setup

```
make setup
```

Build

```
make build
```

Run webpack server

```
make webpack
```

Run Kemal server

```
make kemal
```

Build and run Kemal

```
make prod
```

### Using native commands

#### Develop

Install npm packages

```
npm install
```

Run webpack server

```
npm run dev
```

Build with webpack

```
npm run build
```

#### Run

Install shards

```
shards install
```

Build Kemal

```
shards build
```

Run Kemal

```
bin/app
```