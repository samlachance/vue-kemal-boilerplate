require "kemal"

Kemal.config.powered_by_header = false

get "/" do |env|
  send_file env, "public/index.html"
end

Kemal.run
