prod: build kemal

setup:
	shards install
	npm install

webpack:
	npm run dev

kemal:
	bin/app

build:
	npm run build
	shards build
	